<?php

class Emails extends CI_Controller {

    public function __construct() {
        parent::__construct();
    }

    function index() {
        $data['nombre'] = $this->session->userdata('nombre');
        $view["body"] = $this->load->view("admin/enviarTicket", $data, TRUE);
        $view["title"] = "Enviar Ticket a ";
        $this->parser->parse("template/body", $view);
    }

    function enviar() {
        $this->load->library('email');
        $config['protocol'] = 'smtp';
        $config['smtp_host'] = 'smtp.hostinger.com.ar';
        $config['smtp_port'] = 587;
        $config['smtp_user'] = 'ticket@4topiso.com';
        $config['smtp_pass'] = 'CuartoPiso4';

        $asunto = $this->input->post('asunto');
        $mensaje = $this->input->post('mensaje');

        $this->email->from('ticket@4topiso.com', 'Fer-Pet S.R.L.');
        $this->email->to('ticket@4topiso.com');
        $this->email->cc('pablonello@hotmail.com');
        $this->email->subject($asunto);
        $this->email->message($mensaje);

        if ($this->email->send()) {
            $this->session->set_flashdata('text', 'Se envio correctamente el correo');
            $this->session->set_flashdata('type', 'success');
            redirect('Emails');
        } else {
            $this->session->set_flashdata('text', 'No se envio correctamente el correo');
            $this->session->set_flashdata('type', 'danger');
            redirect('Emails');
        }


        redirect('emails');
    }

}
