<?php if ($error = $this->session->flashdata('Usuario cargado')) { ?>
    <div class="row">
        <div >
            <div class="alert alert-dismissible alert-success">
                <?php echo $error ?>
            </div>
        </div>
    </div>
<?php } else if ($error = $this->session->flashdata('Usuario error')) { ?>
    <div class="row">
        <div >
            <div class="alert alert-dismissible alert-danger">
                <?php echo $error ?>
            </div>
        </div>
    </div>
<?php } ?>

<table class="table table-condensed">
    <tbody>
        <tr>
            <th style="width: 10px">#</th>
            <th>Usario</th>
            <th>Passrowd</th>
            <th>acciones</th>
        </tr>
        <?php foreach ($usuarios as $key => $p) : ?>
            <tr>
                <td><?php echo $p->idusuario ?></td>
                <td><?php echo $p->usuario ?></td>
                <td><?php echo $p->psw ?></td>

                <td>
                    <a class="btn btn-sm btn-primary" 
                       href="<?php echo base_url() . 'Usuarios/user_crud/add' . $p->idusuario ?>">
                        <i class="fa fa-pencil"></i> Editar
                    </a>

                    <a class="btn btn-sm btn-danger" 
                       data-toggle="modal" 
                       data-target="#deleteUsuario"
                       href="#"
                       data-proveedorid="<?php echo $p->idusuario ?>">
                        <i class="fa fa-remove"></i> Eliminar
                    </a>
                </td>

            </tr>
        <?php endforeach; ?>

    </tbody>
</table>

<div class="modal fade" id="deleteUsuario" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">New message</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger" id="borrar-usuario" data-dismiss="modal">Eliminar</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Cancelar</button>
            </div>
        </div>
    </div>
</div>

<script>

    var usuariorid = 0;
    var buttondelete;

// abrir el modal
    $('#deleteUsuario').on('show.bs.modal', function (event) {
        buttondelete = $(event.relatedTarget) // Boton que activa el model
        usuariorid = buttondelete.data('usuarioid') // Extraemos el dato del atributo que definimos en data-proveedorid
        // If necessary, you could initiate an AJAX request here (and then do the updating in a callback).
        // Update the modal's content. We'll use jQuery here, but you could use a data binding library or other methods instead.
        var modal = $(this)
        modal.find('.modal-title').text('Seguro que desea eliminar el Usuario seleccionado ' + usuariorid)
    });

// llamar a eliminar
    $("#borrar-usuario").click(function () {
        $.ajax({
            url: "<?php echo base_url() ?>Usuarios/post_delete/" + usuariorid
        }).done(function (res) {
            if (res == 1) {
                $(buttondelete).parent().parent().remove();//se utilizan dos parent para remover el boton el tr y el td correspondiente 
                modal.find('.modal-title').text('Se elmino correctamente')
            }
        });
    });
</script> 
