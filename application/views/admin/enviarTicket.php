<?php if ($this->session->flashdata('msg')) { ?>
    <?php echo $msg = $this->session->flashdata('msg'); ?>
<?php } ?>

<?php if ($this->session->flashdata('error_msg')) { ?>
    <?php echo $error_msg = $this->session->flashdata('error_msg'); ?>
<?php } ?>

<article class="contenido">
    <form action="Emails/enviar" method="POST">
        <table class="gridtable">
          
            <tr>
                <td>
                    <label lass="label" for="">Asunto</label>
                </td>
                <td>
                    <input type="text" class="form-control" name="asunto" id="asunto" >
                </td>

            </tr>
            <tr>
                <td>
                    <label lass="label" for="mensaje">Mensaje</label>
                </td>
                <td>
                    <textarea cols="35" rows="10" class="form-control" name="mensaje" id="mensaje" ></textarea>
                </td>
            </tr>
            <tr>
                <td colspan="2">
                    <button type="submit" class="btn btn-primary" data-dismiss="modal">Enviar Correo</button>
                </td>
            </tr>
        </table>

    </form>
</article>