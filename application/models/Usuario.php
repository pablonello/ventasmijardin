<?php

define('METHOD', 'AES-256-CBC');
define('SECRET_KEY', '$CARLOS@2016');
define('SECRET_IV', '101712');

class Usuario extends MY_Model {

    public $table = "usuarios";
    public $table_id = "idusuarios";

    public function __construct() {
        parent::__construct();
    }

    public function usuario_por_nombre_contrasena($username, $pswencri) {
 
        $this->db->select('*');
        $this->db->from('usuarios');
        $this->db->where('usuario', $username);
        $this->db->where('psw', $pswencri);
        $consulta = $this->db->get();
       
        $resultado = $consulta->row();

        return $resultado;
    }

    public static function encryption($string) {
        $output = FALSE;
        $key = hash('sha256', SECRET_KEY);
        $iv = substr(hash('sha256', SECRET_IV), 0, 16);
        $output = openssl_encrypt($string, METHOD, $key, 0, $iv);
        $output = base64_encode($output);
        return $output;
    }

    public static function decryption($string) {
        $key = hash('sha256', SECRET_KEY);
        $iv = substr(hash('sha256', SECRET_IV), 0, 16);
        $output = openssl_decrypt(base64_decode($string), METHOD, $key, 0, $iv);
        return $output;
    }

}
